<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');
Route::get('/contact', 'PagesController@contact');



/*

    GET /projects            (index)

    GET /projects/create     (create - show form)

    GET /projects/{id}       (show)

    POST /projects           (store)

    GET /projects/{id}/edit   (edit)

    PATCH/PUT /projects/{id} (update)

    DELETE /projects/{id}    (delete)

*/


/*
 *  To create all the CRUD methods for the controller
 *  php artisan make:controller ProjectsController -r
 *
*/

Route::resource('projects', 'ProjectsController');

//Route::get('/projects', 'ProjectsController@index');
//
//Route::get('/projects/create', 'ProjectsController@create');
//
//Route::get('/projects/{project_id}', 'ProjectsController@show');
//
//Route::post('/projects/store', 'ProjectsController@store');
//
//Route::get('/projects/{project_id}/edit', 'ProjectsController@edit');
//
//Route::patch('/projects/{project_id}', 'ProjectsController@update');
//
//Route::delete('/projects/{project_id}', 'ProjectsController@destroy');



Route::resource('tasks', 'TasksController');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

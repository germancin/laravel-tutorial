@section('title', 'projects')

@extends('menus.project')

@extends('layouts.default')

@section('content')

    @foreach($projects as $project)

        <li>{{$project->title}}</li>

    @endforeach

@endsection

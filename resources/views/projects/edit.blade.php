@extends('layouts.default')


@section('content')

    <br/><h1>Edit project {{$id}}</h1><br/>




    <form method="post" action="/projects/{{$project->id}}">

        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="field">
            <label for="title" class="label">Title</label>

            <div>
                <input class="input" type="text" name="title" value="{{$project->title}}">
            </div>

        </div>

        <div class="field">
            <label for="title" class="label">TitDescriptionle</label>

            <div>
                <textarea class="textarea" rows="4" cols="50" name="description" >{{$project->description}}</textarea>
            </div>

        </div>



        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link" >Update </button>
            </div>
            <div class="control">
                <button type="submit" class="button " >Delete </button>
            </div>

        </div>

    </form>

@endsection

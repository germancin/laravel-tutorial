@section('title', 'create project')

@extends('menus.project')

@extends('layouts.default')


@section('content')

    <h2>Create project</h2>

    @if($errors->any())

        <div class="notification is-danger">

            @foreach($errors->all() as $error )

                {{$error}}

            @endforeach
        </div>

    @endif


    <form method="POST" action="/projects">

        {{ csrf_field() }}

        <div class="field">
            <label for="title" class="label">Title</label>

            <div>
                <input class="input" type="text" name="title"  value="{{ old('title')  }}" >
            </div>

        </div>

        <div class="field">
            <label for="title" class="label">Description</label>

            <div>
                <textarea class="textarea" rows="4" cols="50" name="description"  value="{{ old('description')  }}" ></textarea>
            </div>

        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link" >Save </button>
            </div>

        </div>

    </form>

@endsection




<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Project;

class ProjectsController extends Controller
{
    public function index()
    {

        $projects = Project::all();
        return view('projects.index', compact('projects'));

    }

    public function create()
    {

        return view('projects.create');

    }

    public function store()
    {

        Project::create(request()->validate([

            'title' => ['required', 'min:3', 'max:125 '],
            'description' => 'required'

        ]));

        return redirect('/projects');

    }

    public function edit($id)
    {
        $project = Project::find($id);

        return view('projects.edit', compact('id', 'project'));
    }

    public function update(Project $project)
    {
        $project->update(request(['title', 'description']));

        return redirect('/projects');
    }

    public function destroy($id)
    {

        $project = Project::find($id);

    }

    public function show($id)
    {

        $project = Project::find($id);

        return view('projects.show', compact('project'));

    }
}
